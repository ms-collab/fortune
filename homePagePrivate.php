<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Home Page - Private</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <link href="stylePrivate.css" rel="stylesheet">

        <link href="https://fonts.cdnfonts.com/css/glacial-indifference-2" rel="stylesheet">
    </head>
    <body>
        <div class="sezAlta row">
            <div class="col">
                <img src="docs/logo.png">
            </div>
            <div class="col">
                <form method="POST">
                    <button class='btn-logout' formaction='db.php?action=logout'>LOGOUT</button>
                </form>
            </div>
        </div>
        <div class="pulsanti-sezioni">
            <form id="sezioni-back" method="POST">
                <button id="utenti" formaction="db.php?action=user-list">UTENTI</button>
                <br>
                <button id="categorie" formaction="db.php?action=cat-list">CATEGORIE</button>
                <br>
                <button id="frasi" formaction="db.php?action=frase-list">FRASI</button>       
            </form>
        </div>
    </body>
</html>