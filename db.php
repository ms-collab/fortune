<?php
require('vendor/autoload.php');
require('src/config.php');
require('src/template.php');
require('src/security.php');
session_start();


$db = newAdoConnection('mysqli');
//$db->debug = true;
/** @phpstan-ignore-next-line */
$db->connect($dbHost, $dbUsername, $dbPassword, $dbName);

$action=(isset($_REQUEST['action']))?$_REQUEST['action']:'check-login-step-2';

switch ($action) {
	case 'home':
		include('public/index.php');
		break;
	case 'login':
		checkLogin();
		break;
	case 'user-list':
		include('src/utenti.php');
		listaUtenti($db);
		break;
	case 'user-delete':
		include('src/utenti.php');
		cancellaUtente($db,$_REQUEST['id']);
		break;	
	case 'user-add-step-1':
		include('src/utenti.php');
		formAddUtente();
		break;	
	case 'user-add-step-2':
		include('src/utenti.php');
		saveUtente($db);
		break;	
	case 'user-modify-step-1':
		include('src/utenti.php');
		formModifyUtente($db,$_REQUEST['id']);
		break;						
	case 'user-modify-step-2':
		include('src/utenti.php');
		formModifyUtenteStep2($db,$_REQUEST['id']);
		break;		
	case 'check-login-step-2':
		checkUsernameAndPassword($db,$_REQUEST['NomeUtente'],$_REQUEST['password']);
		break;
	case 'cat-list':
		include('src/categorie.php');
		listaCategoria($db);
		break;
	case 'cat-delete':
		include('src/categorie.php');
		cancellaCategoria($db,$_REQUEST['Tipo']);
		break;	
	case 'cat-add-step-1':
		include('src/categorie.php');
		formAddCategoria();
		break;	
	case 'cat-add-step-2':
		include('src/categorie.php');
		saveCategoria($db);
		break;	
	case 'cat-modify-step-1':
		include('src/categorie.php');
		formModifyCategoria($db,$_REQUEST['Tipo']);
		break;						
	case 'cat-modify-step-2':
		include('src/categorie.php');
		formModifyCategoriaStep2($db,$_REQUEST['Tipo'],$_REQUEST['Tipo2']);
		break;
	case 'frase-list':
		include('src/frasi.php');
		listaFrase($db);
		break;
	case 'frase-delete':
		include('src/frasi.php');
		cancellaFrase($db,$_REQUEST['Contenuto']);
		break;	
	case 'frase-add-step-1':
		include('src/frasi.php');
		formAddFrase();
		break;	
	case 'frase-add-step-2':
		include('src/frasi.php');
		saveFrase($db);
		break;	
	case 'frase-modify-step-1':
		include('src/frasi.php');
		formModifyFrase($db,$_REQUEST['Contenuto']);
		break;						
	case 'frase-modify-step-2':
		include('src/frasi.php');
		formModifyFraseStep2($db,$_REQUEST['Contenuto'],$_REQUEST['Contenuto2']);
		break;		
	case 'logout':
		logout();
		break;
}