<?php


function listaUtenti($db)
{
	$sql = "select * from utente order by NomeUtente";
	$array = $db->getAll($sql);
	inizioPagina();
	echo "<div class='titolo row'>
	<div class='col'>
					<h2>UTENTI</h2>
					</div>
					<div class='col sezLogout'>
					<a href='?action=user-add-step-1' class='btn btn-success'><i class='fa-solid fa-circle-plus'></i></a>
					</div>
				</div>
				<div class='contenuto-db'>";
	echo "<table class='table'><tr><th></th><th>ID</th><th>Username</th></tr>";
	foreach ($array as $key => $riga) {
		echo "<tr>";
		echo '<td>';
		echo '<a class="btn btn-info" href="?action=user-modify-step-1&id=' . $riga['ID'] . '"><i class="fa-regular fa-pen-to-square"></i></a>';

		echo '<a class="btn btn-danger" href="?action=user-delete&id=' . $riga['ID'] . '"><i class="fa-regular fa-trash-can"></i></a>';

		echo '</td>';
		echo "<td>" . $riga['ID'] . '</td>';
		echo "<td>" . $riga['NomeUtente'] . '</td>';

		echo "</tr>";
	}
	echo "</table>";
	finePagina();
}




function cancellaUtente($db, $id)
{
	$sql = "delete from utente where ID=" . $id;
	$db->execute($sql);
	listaUtenti($db);
}


function formAddUtente()
{
	inizioPagina();
?>
	<form action="?action=user-add-step-2" method="POST">
		<label for="NomeUtente">Username:</label>
		<input type="text" id="NomeUtente" name="NomeUtente">
		<label for="password">Password:</label>
		<input type="password" id="password" name="password">
		<input type="submit" value="Aggiungi utente">
	</form>
<?php
	finePagina();
}


function saveUtente($db)
{
	$NomeUtente = $_REQUEST['NomeUtente'];
	$password = md5($_REQUEST['password']);


	$sql = "insert into utente (NomeUtente,Password) values ('$NomeUtente','$password')";
	$db->execute($sql);
	listaUtenti($db);
}


function formModifyUtente($db, $id)
{
	$sql = "select * from utente where ID=" . $id;
	$array = $db->getAll($sql);
	$id = $array[0]['ID'];
	$NomeUtente = $array[0]['NomeUtente'];
	inizioPagina();
?>
	<form action="?action=user-modify-step-2" method="POST">
		<input type="hidden" name="id" value="<?php echo $id ?>">
		<label for="NomeUtente">Username:</label>
		<input type="text" id="NomeUtente" name="NomeUtente" value="<?php echo $NomeUtente ?>">
		<label for="password">Password:</label>
		<input type="password" id="password" name="password">
		<input type="submit" value="Modifica utente">
	</form>
<?php
	finePagina();
}

function formModifyUtenteStep2($db, $id)
{

	$record = array();
	$record['NomeUtente'] = $_REQUEST['NomeUtente'];

	if (strlen($_REQUEST['password']) > 0) {
		$record['password'] = md5($_REQUEST['password']);
	}

	$db->autoExecute('utente', $record, 'UPDATE', 'ID = ' . $id);
	listaUtenti($db);
}
?>