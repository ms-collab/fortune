<?php


function listaCategoria($db)
{
	$sql = "select * from categoria order by Tipo";
	$array = $db->getAll($sql);
	inizioPagina();
	echo "<div class='titolo row'>
	<div class='col'>
					<h2>CATEGORIE</h2>
					</div>
					<div class='col sezLogout'>
					<a href='?action=cat-add-step-1' class='btn btn-success'><i class='fa-solid fa-circle-plus'></i></a>
					</div>
				</div>
				<div class='contenuto-db'>";
	echo "<table class='table'><tr><th></th><th>Tipo</th></tr>";
	foreach ($array as $key => $riga) {
		echo "<tr>";
		echo '<td>';
		echo '<a class="btn btn-info" href="?action=cat-modify-step-1&Tipo=' . $riga['Tipo'] . '"><i class="fa-regular fa-pen-to-square"></i></a>';

		echo '<a class="btn btn-danger" href="?action=cat-delete&Tipo=' . $riga['Tipo'] . '"><i class="fa-regular fa-trash-can"></i></a>';

		echo '</td>';
		echo "<td>" . $riga['Tipo'] . '</td>';

		echo "</tr>";
	}
	echo "</table>";
	finePagina();
}




function cancellaCategoria($db, $Tipo)
{
	$sql = "delete from categoria where Tipo='$Tipo'";
	$db->execute($sql);
	listaCategoria($db);
}


function formAddCategoria()
{
	inizioPagina();
?>
	<form action="?action=cat-add-step-2" method="POST">
		<label for="Tipo">Nome:</label>
		<input type="text" id="Tipo" name="Tipo">
		<input type="submit" value="Aggiungi categoria">
	</form>
<?php
	finePagina();
}


function saveCategoria($db)
{
	$Tipo = $_REQUEST['Tipo'];

	$sql = "insert into categoria (Tipo) values ('$Tipo')";
	$db->execute($sql);
	listaCategoria($db);
}


function formModifyCategoria($db, $Tipo)
{
	$sql = "select * from categoria where Tipo='$Tipo'";
	$array = $db->getAll($sql);
	$Tipo2 = $Tipo;
	$Tipo = $array[0]['Tipo'];
	inizioPagina();
?>
	<form action="?action=cat-modify-step-2" method="POST">
		<input type="hidden" id="Tipo2" name="Tipo2" value="<?php echo $Tipo2?>">
		<label for="Tipo">Nome:</label>
		<input type="text" id="Tipo" name="Tipo" value="<?php echo $Tipo ?>">
		<input type="submit" value="Modifica categoria">
	</form>
<?php
	finePagina();
}

function formModifyCategoriaStep2($db, $Tipo, $Tipo2)
{

	$record = array();
	$record['Tipo'] = $_REQUEST['Tipo'];
	
	$db->autoExecute('categoria', $record, 'UPDATE', "Tipo = '$Tipo2'");
	listaCategoria($db);
}
