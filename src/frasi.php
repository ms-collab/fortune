<?php


function listaFrase($db){
	$sql = "select * from frase order by Contenuto";
	$array = $db->getAll($sql);
	inizioPagina();
	echo "<div class='titolo row'>
	<div class='col'>
					<h2>FRASI</h2>
					</div>
					<div class='col sezLogout'>
					<a href='?action=frase-add-step-1' class='btn btn-success'><i class='fa-solid fa-circle-plus'></i></a>
					</div>
				</div>
				<div class='contenuto-db'>";
echo "<table class='table'><tr><th></th><th>Contenuto</th><th>Autore</th><th>Categoria</th><th>Chi l'ha aggiunta</th><th>Visualizzabile</th></tr>";
 	foreach ($array as $key => $riga) {
 		echo "<tr>";
		echo '<td>';
		echo '<a class="btn btn-info" href="?action=frase-modify-step-1&Contenuto='.$riga['Contenuto'].'"><i class="fa-regular fa-pen-to-square"></i></a>';

		echo '<a class="btn btn-danger" href="?action=frase-delete&Contenuto='.$riga['Contenuto'].'"><i class="fa-regular fa-trash-can"></i></a>';

 		echo '</td>';
 		echo "<td>".$riga['Contenuto'].'</td>';
        echo "<td>".$riga['Autore'].'</td>';
        echo "<td>".$riga['Categoria'].'</td>';
        echo "<td>".$riga['ChiAggiunto'].'</td>';
        echo "<td>".$riga['Visualizza'].'</td>';
 		
 		
 		echo "</tr>";
 	}
 	echo "</table>";
 	finePagina();
}




function cancellaFrase($db,$Contenuto){
	$sql = "delete from frase where Contenuto='$Contenuto'";
	$db->execute($sql);
	listaFrase($db);
}


function formAddFrase(){
	inizioPagina();
	?>
	<form action="?action=frase-add-step-2" method="POST">
		<label for="Contenuto">Contenuto:</label>
		<input type="text" id="Contenuto" name="Contenuto">     
		<label for="Autore">Autore:</label>
		<input type="text" id="Autore" name="Autore">      
		<label for="Categoria">Categoria:</label>
		<input type="text" id="Categoria" name="Categoria">       
		<label for="Chi">Chi l'ha aggiunta:</label>
		<input type="text" id="Chi" name="Chi">       
		<label for="Visualizza">Visualizza:</label>
		<input type="checkbox" id="Visualizza" name="Visualizza">
		<input type="submit" value="Aggiungi frase">
	</form>
	<?php
	finePagina();
}


function saveFrase($db){
	$Contenuto=$_REQUEST['Contenuto'];
    $Autore =$_REQUEST['Autore'];
    $Categoria = $_REQUEST['Categoria'];
    $Chi = $_REQUEST['Chi'];
    $Visualizza = $_REQUEST['Visualizza'];

	if($Visualizza == 'on') {
		$Visualizza = 1;
	}else {
		$Visualizza = 0;
	}

	$sql = "insert into frase (Contenuto, Autore, Categoria, ChiAggiunto, Visualizza) values ('$Contenuto', '$Autore', '$Categoria', '$Chi', $Visualizza)";
	$db->execute($sql);
	listaFrase($db);
}


function formModifyFrase($db,$Contenuto){
	$sql = "select * from frase where Contenuto='$Contenuto'";
	$array = $db->getAll($sql);
	$Contenuto2 = $Contenuto;
	$Contenuto = $array[0]['Contenuto'];
    $Autore = $array[0]['Autore'];
    $Categoria = $array[0]['Categoria'];
    $Chi = $array[0]['ChiAggiunto'];
    $Visualizza = $array[0]['Visualizza'];
	inizioPagina();
	?>
	<form action="?action=frase-modify-step-2" method="POST">
		<input type="hidden" id="Contenuto2" name="Contenuto2" value="<?php echo $Contenuto2 ?>">
		<label for="Contenuto">Contenuto:</label>
		<input type="text" id="Contenuto" name="Contenuto"  value="<?php echo $Contenuto ?>">
        <label for="Autore">Autore:</label>
		<input type="text" id="Autore" name="Autore" value="<?php echo $Autore ?>">      
		<label for="Categoria">Categoria:</label>
		<input type="text" id="Categoria" name="Categoria" value="<?php echo $Categoria ?>">       
		<label for="Chi">Chi l'ha aggiunta:</label>
		<input type="text" id="Chi" name="Chi" value="<?php echo $Chi ?>">       
		<label for="Visualizza">Visualizza:</label>
		<input type="boolean" id="Visualizza" name="Visualizza" value="<?php echo $Visualizza ?>">
		<input type="submit" value="Modifica frase">
	</form>
	<?php
	finePagina();
}

function formModifyFraseStep2($db,$Contenuto,$Contenuto2){

$record = array();
$record['Contenuto']=$_REQUEST['Contenuto'];
$record['Autore']=$_REQUEST['Autore'];
$record['Categoria']=$_REQUEST['Categoria'];
$record['Chi']=$_REQUEST['Chi'];
$record['Visualizza']=$_REQUEST['Visualizza'];

$db->autoExecute('frase', $record, 'UPDATE', "Contenuto = '$Contenuto2'"); 
	listaFrase($db);
}