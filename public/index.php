<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Home Page - Fortune</title>
        
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="https://fonts.cdnfonts.com/css/glacial-indifference-2" rel="stylesheet">
        
        <style>
            :root {
                --bg-login: #000000;
                --btn-txt: #FFFFFF;
                --bordo: #EBEBEB;
                --bt-txt-hover: #C4761C;
            }

            body {
                background-color: var(--bg-login);
            }
            
            .centrato {
                text-align: center;
                display: flex;
                justify-content: center;
                align-items: center;
            }

            img {
                margin: 20px 0 20px 20px;
                width: 150px;
                height: 150px;
            }

            .btn {
                border-style: solid;
                border-radius: 5px;
                border-width: 3px;
                border-color: var(--bordo);
                color: var(--btn-txt);
                background-color: var(--bg-login);
                margin-top: 30px;
                margin-left: 50%;
            }

            .btn:hover {
                color: var(--bt-txt-hover);
                border-color: var(--bt-txt-hover);
            }

            .col {
                width: 100%;
            }

            .row {
                width: 100%;
            }

            #randomTxt {
                margin-top: 30px;
                margin-right: 20%;
                margin-left: 20%;

                color: var(--bordo);
            }

            .reload {
                height: 100%;
                width: 50px;
                margin-left: 0;

                border-style: solid;
                border-radius: 5px;
            }

            .frase {
                border-style: solid;
                border-radius: 5px;
                border-width: 3px;
                border-color: var(--bordo);

                text-align: center;
                font-family: 'Glacial Indifference', sans-serif;
                font-weight: 400;

                width: 550px;
                height: 300px;

                padding-top: 50px;
            }

            .p2 {
                max-width: fit-content;
            }

            .contenuto {
                font-size: 22px;
                font-weight: 700;
            }

            .autore {
                font-size: 18px;
            }

            .categoria {
                font-size: 15px;
            }

            p {
                margin-top: 5px;
            }
        </style>
    </head>
    <body>
        <div class="sezioneHeader row">
            <div class="col"><img src="../docs/logo.png"></div>
            <div class="col"><a class="btn" href="../login.html">Area Privata</a></div>
        </div>
            <form action="index.php" class="centrato" id="randomTxt">
                <?php
                /*codice dove verrà inserito la logica per far comparire la scritta casuale. Sfondo nero scritta bianca
                centrato nella pagina, e se si riesce con animazione*/
                require('../vendor/autoload.php');
                require('../src/config.php');
                session_start();

                $db = newAdoConnection('mysqli');

                $db->connect($dbHost, $dbUsername, $dbPassword, $dbName);

                $sql = "select * from frase where Visualizza = 1 order by Contenuto";
	            $array = $db->getAll($sql);

                $cnt = rand(0, sizeof($array)-1);

                $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                if(strpos($url, '?api-random')) {
                    $result = $array[$cnt];
                    $file = json_encode($result);
                    file_put_contents("frase.json", $file);

                    header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
                    header("Cache-Control: public"); // needed for internet explorer
                    header("Content-Type: application/json");
                    header("Content-Transfer-Encoding: Binary");
                    //header("Content-Length:".filesize("frase.json"));
                    header("Content-Disposition: attachment; filename=frase.json");
                    readfile("frase.json");
                    die();        
                } 

                $Contenuto = $array[$cnt]['Contenuto'];
                $Autore = $array[$cnt]['Autore'];
                $Categoria = $array[$cnt]['Categoria'];

                echo "<div class='row'>
                    <div class='frase col'>
                        <p class='contenuto'><i>".$Contenuto."</i></p>
                        <p class='autore'>CIT ''".$Autore."''</p>
                        <p class='categoria'>".$Categoria."</p>
                    </div>
                    <div class='col p2'>
                        <button class='reload'><i class='fa-solid fa-rotate-right'></i></button>
                    </div>
                </div>"
            ?>
            
            </form>
    </body>
</html>
