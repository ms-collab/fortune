-- --------------------------------------------------------

--
-- Struttura della tabella `categoria`
--

CREATE TABLE `categoria` (
  `Tipo` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `frase`
--

CREATE TABLE `frase` (
  `Contenuto` varchar(255) NOT NULL,
  `Autore` varchar(20) DEFAULT NULL,
  `ChiAggiunto` varchar(20) DEFAULT NULL,
  `Visualizza` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `NomeUtente` varchar(20) DEFAULT NULL,
  `Password` varchar(20) DEFAULT NULL,
  `ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
